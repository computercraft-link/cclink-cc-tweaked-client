local helpers = require("helpers")

local SETTINGS = {
	["ccLink.uuid"] = {
		description = "The UUID of this machine assigned by the VS Code extension",
		type = "string",
	},
	["ccLink.url"] = {
		description = "The URL of the machine running the VS Code extension",
		type = "string",
	},
	["ccLink.reconnectInterval"] = {
		description = "How many seconds should be in between reconnection attempts",
		type = "number",
		default = 5
	}
}

---Define the settings for ccLink
local function defineSettings()
	local names = settings.getNames()

	for settingName, settingData in pairs(SETTINGS) do
		if not helpers.includes(names, settingName) then
			settings.define(settingName, settingData)
		end
	end
end

---Get all of the settings for this program
local function getSettings()
	local s = {}
	for name, data in pairs(SETTINGS) do
		s[string.gsub(name, "ccLink.", "")] = settings.get(name)
	end

	return s
end

return { SETTINGS = SETTINGS, defineSettings = defineSettings, getSettings = getSettings }
