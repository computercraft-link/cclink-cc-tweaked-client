local pretty = require("cc.pretty")

local operations = {}

---Write to a file
---@param ws Websocket The websocket in use
---@param payload {operation: "write", path: string, content: string}
---@return string? error If an error occurred, the reason
function operations.write(ws, payload)
	local path = payload.path
	local content = payload.content

	local file, err = fs.open(path, "w")
	if not file then
		return err
	end
	file.write(content)
	file.close()

	pretty.print(pretty.concat(pretty.text("Successfully wrote to ", colors.green), pretty.text(path, colors.magenta)))
end

---Delete a file
---@param ws Websocket The websocket in use
---@param payload {operation: "delete", path: string}
---@return string error If an error occurred, the reason
function operations.delete(ws, payload)
	local path = payload.path

	if not fs.exists(path) then
		return path .. " does not exist"
	end

	fs.delete(path)

	pretty.print(pretty.concat(pretty.text("Successfully deleted ", colors.green), pretty.text(path, colors.magenta)))
end

---Rename a file. This deletes the old one and replaces it with a new one
---@param ws Websocket The websocket in use
---@param payload {operation: "rename", oldPath: string, newPath: string}
---@return string error If an error occurred, the reason
function operations.rename(ws, payload)
	local oldPath = payload.oldPath
	local newPath = payload.newPath

	if not fs.exists(oldPath) then
		return oldPath .. " does not exist"
	end

	local source = fs.open(oldPath, "r")
	---@cast source ReadHandle
	local contents = source.readAll()
	source.close()
	fs.delete(oldPath)

	local target = fs.open(newPath, "w")
	---@cast target WriteHandle
	target.write(contents)
	target.close()

	pretty.print(
		pretty.concat(
			pretty.text("Successfully renamed ", colors.green),
			pretty.text(oldPath, colors.magenta),
			pretty.text(" to ", colors.green),
			pretty.text(newPath, colors.magenta)
		)
	)
end

---Get a list of this computer's directories
---@param ws Websocket The websocket in use
---@param payload nil
---@return string error If an error occurred, the reason
function operations.dirTree(ws, payload)
	local function parseTree(path, route)
		local children = {}

		for k, v in ipairs(path) do
			local fullPath = fs.combine(route, "/" .. v)
			local isDir = fs.isDir(fullPath)
			if isDir then
				children[v] = parseTree(fs.list(fullPath), fullPath)
			else
				children[v] = true
			end
		end
		return children
	end

	local tree = {}

	local list = fs.list("/")
	tree = parseTree(list, "/")

	local payload = {
		operation = "dirTree",
		tree = tree,
	}

	ws.send(textutils.serializeJSON(payload))
end

return operations
