local pretty = require("cc.pretty")

local helpers = require("programFiles.helpers")
local ccLinkSettings = require("programFiles.settings")

local operations = require("programFiles.operations")

local args = { ... }
local pathToHere = shell.getRunningProgram()

----------------------------------[[ SETUP ]]----------------------------------

ccLinkSettings.defineSettings()

-- Help file setup
local helpPath = help.path()
local pathToHelp = fs.combine(fs.getDir(pathToHere), "help/")

if not string.find(helpPath, pathToHelp) then
	help.setPath(helpPath .. ":" .. pathToHelp)
end

-- Add to path
local programsPath = shell.path()
if not string.find(programsPath, pathToHere) then
	shell.setPath(programsPath .. ":" .. pathToHere)
end

---------------------------------[[ SETTINGS ]]--------------------------------

local settingValues = ccLinkSettings.getSettings()

--------------------------------[[ ARG PARSING ]]------------------------------

local UUID_PATTERN = "%w%w%w%w%w%w%w%w%-%w%w%w%w%-%w%w%w%w%-%w%w%w%w%-%w%w%w%w%w%w%w%w%w%w%w%w"

---@type table<string, flag>
local FLAGS = {
	uuid = {
		type = "string",
		pattern = UUID_PATTERN,
	},
	url = {
		type = "string",
	},
}

local flags, err = helpers.parseFlags(FLAGS, args)

if err then
	pretty.print(err)
	return
end

local url = flags and flags.url or settingValues.url
local uuid = flags and flags.uuid or settingValues.uuid

if not url then
	printError("A URL is required to connect to ccLink")
	return
end

if not uuid then
	printError("A UUID is required to connect to ccLink")
	return
end

-----------------------------[[ WEBSOCKET SETUP ]]-----------------------------

local label = os.getComputerLabel() or string.format("Computer #%i", os.getComputerID())

local headers = { Authorization = uuid, ["computer-name"] = label, ["Sec-Websocket-Protocol"] = "computercraft-link" }

http.websocketAsync(url, headers)

------------------------------[[ WEBSOCKET LOOP ]]-----------------------------

---@type Websocket|nil
local ws

local reconnectionAttemptTimer

local function onTerminate()
	-- cancel any reconnection attempts
	if reconnectionAttemptTimer then
		os.cancelTimer(reconnectionAttemptTimer)
	end

	-- close websocket
	if ws then
		ws.close()
		ws = nil
		printError("Websocket Closed")
	end
	printError("Terminated")
end

local function onFailure(URL, reason)
	if URL ~= url then
		return
	end

	if ws and ws.close then
		ws.close()
	end
	ws = nil

	printError(reason)

	if not reconnectionAttemptTimer then
		os.queueEvent("terminate")
	end
end

---@param URL string
---@param websocket Websocket
local function onSuccess(URL, websocket)
	if URL ~= url then
		return
	end

	pretty.print(pretty.concat(pretty.text("Connected to ", colors.green), pretty.text(URL, colors.magenta)))

	ws = websocket

	if reconnectionAttemptTimer then
		os.cancelTimer(reconnectionAttemptTimer)
	end
end

local function onClosed(URL, reason)
	if URL ~= url then
		return
	end

	pretty.print(pretty.concat(pretty.text("Websocket Disconnect: ", colors.red), pretty.text(URL, colors.magenta)))
	printError(reason)

	if reason == "The connection has been closed manually" then
		return os.queueEvent("terminate")
	end

	if ws then
		reconnectionAttemptTimer = os.startTimer(settingValues.reconnectInterval)

		pretty.print(
			pretty.concat(
				pretty.text("Attempting to reconnect to ", colors.orange),
				pretty.text(url, colors.magenta),
				pretty.text(" in ", colors.orange),
				pretty.text(tostring(settingValues.reconnectInterval), colors.lightBlue),
				pretty.text(" seconds", colors.orange)
			)
		)
	end

	ws = nil
end

---@param URL string
---@param message string
---@param isBinary boolean
local function onMessage(URL, message, isBinary)
	if URL ~= url then
		return
	end

	local payload, err = textutils.unserializeJSON(message)

	if not payload then
		printError(err)
		return
	end

	local operation = payload.operation

	local error = helpers.switch(operation, operations, ws, payload)
	if error then
		printError(error)
	end
end

local function onTimer(ID)
	if not ws then
		if ID == reconnectionAttemptTimer then
			http.websocketAsync(url, headers)

			reconnectionAttemptTimer = os.startTimer(settingValues.reconnectInterval)

			pretty.print(
				pretty.concat(
					pretty.text("Attempting to reconnect to ", colors.orange),
					pretty.text(url, colors.magenta),
					pretty.text(" in ", colors.orange),
					pretty.text(tostring(settingValues.reconnectInterval), colors.lightBlue),
					pretty.text(" seconds", colors.orange)
				)
			)
		end
	end
end

while true do
	local event = { os.pullEventRaw() }

	local type = table.remove(event, 1)

	local cases = {
		terminate = onTerminate,
		websocket_failure = onFailure,
		websocket_success = onSuccess,
		websocket_closed = onClosed,
		websocket_message = onMessage,
		timer = onTimer,
	}

	helpers.switch(type, cases, table.unpack(event))

	if type == "terminate" then
		break
	end
end
